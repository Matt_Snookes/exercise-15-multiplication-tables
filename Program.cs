﻿using System;

namespace exercise_15
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
             int n, i;
                Console.WriteLine("Enter a number to find the multiplication table\n");
                n = Int32.Parse(Console.ReadLine());
                for (i = 1; i <= 12; ++i)
                {
                    Console.WriteLine(n + " * " + i + " = " + n * i);
                }
                       
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
